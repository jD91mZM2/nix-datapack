{
  function.cool = {
    tags.minecraft = [ "tick" ];

    setup = ''
      team add cool
      team modify cool color black
    '';

    teardown = ''
      team remove cool
    '';

    code = ''
      team join cool @a
    '';
  };
}
