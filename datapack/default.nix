{ lib, ... }:
{
  datapack = {
    name = "my-datapack";
    description.text = "My custom datapack";
  };

  imports = [
    ./functions
  ];
}
