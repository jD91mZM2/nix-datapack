{
  description = "Minecraft datapack written with nix";

  inputs = {
    utils.url = "github:numtide/flake-utils";
    nix-exprs.url = "gitlab:jD91mZM2/nix-exprs";
  };

  outputs = { self, nixpkgs, utils, nix-exprs }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages."${system}";
      in
      rec {
        # `nix build`
        packages.datapack = nix-exprs.builders."${system}".minecraft.mkDatapack ./datapack;
        defaultPackage = packages.datapack;

        # `nix run`
        apps.datapack = utils.lib.mkApp {
          drv = packages.datapack.installer;
        };
        defaultApp = apps.datapack;
      }
    );
}
