# My datapack

[![built with nix](https://builtwithnix.org/badge.svg)](https://builtwithnix.org)

This is a template repository for your custom datapacks, that are highly structured because they are built with Nix!!

## Built with Nix... how?

[My nix expressions repository](https://gitlab.com/jD91mZM2/nix-exprs/) has a
Minecraft Datapack builder. It creates your datapack in a nice `.zip` file, and
an installer along with it. This repository is simply an example user of this
feature.
